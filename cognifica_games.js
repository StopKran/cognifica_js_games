
function OnceClickGameSettings (width, height, length, show_welcome, welcome_text, text_style, empty_interval, border_interval, whole_interval) {
    this.width = width;
    this.height = height;
    this.length = length;
    this.show_welcome = show_welcome;
    this.welcome_text = welcome_text;
    this.text_style = text_style;
    this.empty_interval = empty_interval;
    this.border_interval = border_interval;
    this.whole_interval = whole_interval;
}

