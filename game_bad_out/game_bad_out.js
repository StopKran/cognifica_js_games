//settings

var text_style = { font: "32px Calibri", fill: "#000000", align: "center" };


//constants

//const-var

//results

//game variables
var csprite = 0;

function CircleSprite(image){
    this.x = 0;
    this.y = 0;
    this.sprite = game.add.sprite(this.x-20,this.y, image.imageName);
    this.isGood = image.isGood;
    this.sprite.scale.set(0.1);
    this.mask = game.add.graphics(this.x, this.y);
    this.mask.beginFill(0xffffff);
    this.mask.drawCircle(this.x + 50, this.x + 50, 100);
    this.sprite.mask = this.mask;
    if (image.isGood){
        this.circle = game.add.sprite(this.x, this.y+0, "circle_good");
    }else {
        this.circle = game.add.sprite(this.x, this.y+0, "circle_bad");
    }
}
Object.defineProperties(CircleSprite, {
    "x": { set: function (x) {
        this.sprite.x = x;
        this.mask.x = x;
        this.circle.x = x;
        this.x = x;
    }},
    "y": { set: function (y) {
        this.sprite.y = y;
        this.mask.y = y;
        this.circle.y = y;
        this.y = y;
    }}
});


var game = new Phaser.Game(800, 600, Phaser.AUTO, '', {
    preload: preload,
    create: create,
    update: update
});
function preload() {
    game.load.image("circle_bad", "assets/circle_bad.png");
    game.load.image("circle_good", "assets/circle_good.png");
    load_images();
}
function create() {
    csprite = new CircleSprite(get_random_image());
}
function update() {
    csprite.x += 1;

}
function generate_sprite(){
    var image = get_random_image();
    var new_sprite = game.add.sprite(0, 0, image.imageName);
    new_sprite.scale.set(0.1);
    mask = game.add.graphics(0, 0);
    mask.beginFill(0xffffff);
    mask.drawCircle(70, 50, 100);
    new_sprite.mask = mask;

    circle = game.add.sprite(20, 0, "circle_bad");

    sprite = new_sprite;
}

