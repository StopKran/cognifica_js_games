//settings
var game_width = 800;
var game_height = 600;

var show_tutorial = true;

var game_length = 20;


var empty_interval = 400;
var border_interval = 150;
var whole_interval = 1000;

var welcome_text = "Привет!\n\nСейчас тебе предстоит путешествие в мир еды!\nНажимай на хорошую пищу, что бы съесть её\nи старайся не есть плохую пищу.\n\n" +
    "Тебе будут даваться подсказки\nв виде прямоугольных рамок.\n\nПриступим?";

var text_style = { font: "32px Calibri", fill: "#000000", align: "center" };

var settings = new OnceClickGameSettings(800,600,20,true,welcome_text,text_style, empty_interval, border_interval, whole_interval);

//constants



//const-var
var background;
var text;

//results
var good_right = 0;
var bad_right = 0;
var good_wrong = 0;
var bad_wrong = 0;

//game variables
var stage = "start"; //start, countdown, game, finish



var game = new Phaser.Game(settings.width, settings.height, Phaser.AUTO, '', { preload: preload, create: create, update: update });

function preload() {
    load_images();
}

function create() {
    game.stage.setBackgroundColor(0xd5f6fb);
    if(settings.show_welcome == true) {
        start_stage();
    }else{
        change_to_countdown();
    }
}

function update () {

}

//start stage
function start_stage(){
    text = game.add.text(game.world.centerX, game.world.centerY, welcome_text, text_style);
    text.anchor.set(0.5);
    game.input.onDown.addOnce(change_to_countdown, this);
}

function change_to_countdown(){
    stage = "countdown";
    text.text = "";
    countdown_count = 3;
    start_countdown();
}


//countdown
var countdown_count = 0;
var countdown_text;
function start_countdown(){
    countdown_text = game.add.text(game.world.centerX, game.world.centerY, "", text_style);
    update_countdown();
    game.time.events.repeat(Phaser.Timer.SECOND, 3, update_countdown, this);
    game.time.events.add(Phaser.Timer.SECOND * 3, start_game, this);
}

function update_countdown(){
    if(countdown_count == 0){
        countdown_text.text = "";
        return;
    }
    countdown_text.text = countdown_count;
    countdown_count = countdown_count - 1;
}

//game stage

function start_game(){
    stage = "game";
    next_image();
}

var showed = 0;
var current_click_event;
var border_event;
var current_sprite;
var current_image;
var end_time_event;
var next_image_event;

function next_image(){
    if (showed >= settings.length){
        end_game();
        return;
    }
    showed = showed + 1;
    current_image = get_random_image();
    current_sprite = game.add.sprite(game.world.centerX, game.world.centerY, current_image.imageName);
    current_sprite.anchor.setTo(0.5, 0.5);

    border_event = game.time.events.add(settings.border_interval, show_border, this);
    current_click_event = game.input.onDown.addOnce(click_event, this);
    end_time_event = game.time.events.add(whole_interval, image_end_time, this);
}

function image_end_time(){
    current_sprite.destroy();
    game.debug.geom(0, 0);
    if (current_image.isGood){
        good_wrong += 1;
    }else{
        bad_right += 1;
    }
    next_image_event = game.time.events.add(settings.empty_interval, next_image, this);
}

function click_event(){
    game.time.events.remove(end_time_event);

    if (current_image.isGood){
        good_right += 1;
    }else{
        bad_wrong += 1;
    }
    current_click_event = 0;
    current_sprite.destroy();
    game.debug.geom(0, 0);
    next_image_event = game.time.events.add(settings.empty_interval, next_image, this);
}

var bottom_border; //todo: refactor to object
var top_border;
var left_border;
var right_border;
var border_color;


function show_border(){
    if (current_image.isGood){
        border_color = '#09DD21'
    }else{
        border_color = '#EE0112'
    }
    var border_width = (game.world.height + game.world.width) * 0.03;

    left_border = new Phaser.Rectangle(0, 0, border_width, game.world.height);
    top_border = new Phaser.Rectangle(0, 0, game.world.width, border_width);
    right_border = new Phaser.Rectangle(game.world.width - border_width, 0, border_width, game.world.height);
    bottom_border = new Phaser.Rectangle(0, game.world.height - border_width, game.world.width, border_width);

    game.debug.geom(left_border,border_color);
    game.debug.geom(top_border,border_color);
    game.debug.geom(right_border,border_color);
    game.debug.geom(bottom_border,border_color);
}

//end_game

function end_game(){

    var end_text = "Хорошей еды съедено: " + good_right +
        "\nВредной еды съедено: " + bad_wrong +
        "\nНе допущенно в рот вредной еды: " + bad_right +
        "\nПропущенно хорошей еды: " + good_wrong;

    text = game.add.text(game.world.centerX, game.world.centerY, end_text, text_style);
    text.anchor.set(0.5);
}
