var bad_images = ["assets/bad/b1.jpg", "assets/bad/b2.jpg", "assets/bad/b3.jpg", "assets/bad/b4.jpg", "assets/bad/b5.jpg", "assets/bad/b6.jpg", "assets/bad/b7.jpg", "assets/bad/b8.jpg", "assets/bad/b9.jpg", "assets/bad/b10.jpg"];
var good_images = ["assets/good/g1.jpg", "assets/good/g2.jpg", "assets/good/g3.jpg", "assets/good/g4.jpg", "assets/good/g5.jpg", "assets/good/g6.jpg", "assets/good/g7.jpg", "assets/good/g8.jpg", "assets/good/g9.jpg", "assets/good/g10.jpg"];

var good_names = ["g1", "g2", "g3", "g4", "g5", "g6", "g7", "g8", "g9", "g10"];
var bad_names = ["b1", "b2", "b3", "b4", "b5", "b6", "b7", "b8", "b9", "b10"];


function GameImage(location, isGood){
    this.imageName = location;
    this.isGood = isGood;
}

function get_random_image(){
    var good = Math.random();
    if (good >=0.5){
        var img_num = Math.round(Math.random() * (good_images.length - 1));
        return new GameImage(good_names[img_num], true);
    }else {
        var img_num = Math.round(Math.random() * (bad_images.length - 1));
        return new GameImage(bad_names[img_num], false);
    }
}

function load_images(){
    for (var i = 0; i < good_images.length; ++i) {
        game.load.image(good_names[i], good_images[i]);
    }

    for (var i = 0; i < bad_images.length; ++i) {
        game.load.image(bad_names[i], bad_images[i]);
    }
}



